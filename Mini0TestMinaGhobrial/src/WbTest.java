import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import java.awt.Component;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class WbTest extends JFrame {

	private JPanel contentPane;
	private JTextField txtSalut;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					WbTest frame = new WbTest();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public WbTest() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		txtSalut = new JTextField();
		txtSalut.setHorizontalAlignment(SwingConstants.CENTER);
		txtSalut.setAlignmentY(Component.TOP_ALIGNMENT);
		txtSalut.setAlignmentX(Component.RIGHT_ALIGNMENT);
		txtSalut.setText("SALUT");
		txtSalut.setBounds(43, 63, 322, 140);
		contentPane.add(txtSalut);
		txtSalut.setColumns(10);
		
		JButton btnBienvenue = new JButton("CLICK");
		btnBienvenue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				JOptionPane.showMessageDialog(null,"Bienvenue !");
				
			}
		});
		btnBienvenue.setBounds(42, 26, 154, 23);
		contentPane.add(btnBienvenue);
	}
}
